;#!/usr/local/bin/sbcl --script
;;;;   Hey, Emacs, this is a -*- Mode: Lisp; Syntax: Common-Lisp -*- file!
;;;;
;;;;   Name:               leap.lisp
;;;;
;;;;   Started:            2022-07-17
;;;;   Modifications:
;;;;
;;;;   Purpose:
;;;;     Exercism demo
;;;;
;;;;   Calling Sequence:
;;;;
;;;;
;;;;   Inputs:
;;;;
;;;;   Outputs:
;;;;
;;;;   Example:
;;;;
;;;;   Notes:
;;;;   Gregorian leap years occur
;;;;     on every year that is evenly divisible by 4
;;;;      except every year that is evenly divisible by 100
;;;;      unless the year is also evenly divisible by 400
;;;;

(cl:defpackage :leap
  (:use :common-lisp)
  (:export :leap-year-p))

(in-package :leap)

(defun leap-year-p (year)
  (and (divisible-by-four-p year)
       (or (not (divisible-by-one-hundred-p year))
           (divisible-by-four-hundred-p year))))

(defun divisible-by-four-hundred-p (n) (zerop (rem n 400)))
(defun divisible-by-one-hundred-p (n) (zerop (rem n 100)))
(defun divisible-by-four-p (n) (zerop (rem n 4)))
